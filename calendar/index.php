<html lang="pl">
    <head>
        <title>Kalendarz <?php echo date("Y"); ?></title>
        <link rel="icon" href="https://www.flaticon.com/svg/vstatic/svg/3143/3143636.svg?token=exp=1613982456~hmac=a3f2c0b10376d7f35719c56a2dcd9e04">
        <style>
            html{
                background-color:rgb(50,50,50);
                color:white;
                font-family: Arial, Helvetica, sans-serif;
                font-size:15px;
                text-align:center;
            }
            table,td,th,tr{
                border:3px solid rgb(0,80,120);
                padding:10px;
                text-align:center;
            }
            td,th{
                width:1%;
            }
            th:hover,td:hover{
                border:3px solid red;
            }
            body{
                margin-left:25%;
                margin-right:25%;
                width:50%;
            }
            .fastlist{
                position:fixed;
                top:50%;
                left:1%;
                border:5px solid rgb(0,80,150);
                width:max-content;
                padding:10px;
                transform:translate(0%,-50%);
                text-align:left;
            }
            a{
                color:white;
                font-size:20px;
            }
            a:hover{
                filter:brightness(80%);
            }
        </style>
    </head>
    <body>
<?php
    $year = date("Y");
    $tablicaNazwMiesiaca = ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'];
    echo "<div class=\"fastlist\"><lu>";
    for($i=0;$i<=11;$i++){
        echo "<li><a href=\"#".($i+1)."\">".$tablicaNazwMiesiaca[$i]."</a></li>";
    }
    echo "</lu></div>";
    for($i=1;$i<=12;$i++){
        $miesiac = date("F",mktime(0,0,0,$i,1,$year));
        echo "<div id=\"".$i."\"><h3>".$tablicaNazwMiesiaca[$i-1]."</h3>";
        echo "<table><tr><th>Poniedziałek</th><th>Wtorek</th><th>Środa</th><th>Czwartek</th><th>Piątek</th><th>Sobota</th><th>Niedziela</th></tr><tr>";
        for($j=1;$j<=32;$j++){
            $date = mktime(0,0,0,$i,$j,$year);
            if($miesiac==date("F",$date)){
                if(date("D",$date)=="Tue"&&date("d",$date)==1){for($a=1;$a<=1;$a++){echo "<td></td>";}}
                if(date("D",$date)=="Wed"&&date("d",$date)==1){for($a=1;$a<=2;$a++){echo "<td></td>";}}
                if(date("D",$date)=="Thu"&&date("d",$date)==1){for($a=1;$a<=3;$a++){echo "<td></td>";}}
                if(date("D",$date)=="Fri"&&date("d",$date)==1){for($a=1;$a<=4;$a++){echo "<td></td>";}}
                if(date("D",$date)=="Sat"&&date("d",$date)==1){for($a=1;$a<=5;$a++){echo "<td></td>";}}
                if(date("D",$date)=="Sun"&&date("d",$date)==1){for($a=1;$a<=6;$a++){echo "<td></td>";}}
                echo "<td>".date("d",$date)."</td>";
            }
            if(date("D",$date)=="Sun"){
                echo "</tr><tr>";
            }
        }
        echo "</tr></table></div>";
    }
?>
    </body>
</html>
